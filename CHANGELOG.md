# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.0] - 2022-01-19

### Fixed

- Time accuracy by using `GLib.get_monotonic_time`.

## [1.0.0] - 2020-08-10

Initial release.

