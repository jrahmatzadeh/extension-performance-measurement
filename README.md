# Performance Measurement for GNOME Shell Extensions

With Performance Measurement tool you can measure the performance of
your GNOME Shell Extension code.

Sample output:

```
------------------------------------------
PERFORMANCE MEASUREMENT
------------------------------------------
Extension Name: My Extension
Block ID: TEST
Microseconds: 29065
Milliseconds: 29.065
Memory Usage: 258.05 KB
```

## Dependency

To use this library you need _libgtop2_ package installed on your system.

Ubuntu:

```bash
$ sudo apt install libgtop2-dev
```

Fedora:

```bash
$ sudo dnf install libgtop2-devel
```

## How to Use

Import _performance.js_ file to your GNOME Shell Extension folder and
then use `start` and `end` functions to measure the block performance:

```javascript
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Performance = Me.imports.performance;

Performance.start('Test1');
// code to measure
Performance.end();
```

You can also use it in nested blocks:

```javascript
Performance.start('Test1');
// code lines to measure for Test1
  Performance.start('Test2');
  // code lines to measure for Test2
  Performance.end(); 
// code lines to measure for Test1
Performance.end();
```

Now you have the performance result in log:

```bash
$ journalctl -fo cat /usr/bin/gnome-shell
```

or 

```bash
$ journalctl -f
```

## License

This Software has been released under GPL version 3 License.
